<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Primera ruta de get 
Route::get('hola',function(){
    echo"Hola estoy en laravel";

});

Route::get('arreglo',function(){
//crear un arreglo de estudiante

//var_dump($estudiantes);

$estudiantes = ["AD" => "Andres",
                "LA" =>"Laura", 
                "JE" =>"Jeronimo",
                "IA" => "Ian"];
//recorrer un arreglo


      foreach($estudiantes as $indice => $estudiante){
        echo "$estudiante tiene indice $indice <hr/>";
}
 
});


Route::get('paises',function(){
   
    //crear un arreglo con información de paises
    $paises = [
        "Colombia" =>[ 
            "capital" => "Bogota",
            "moneda"  =>  "Pesos",
            "poblacion"  => 50.372
                ], 
        
        "Ecuador" =>[ 
            "capital" =>  "Quito",
            "moneda"  =>  " Dolar",
            "poblacion"  =>  17.517
                   ],
        "Brazil" =>[
            "capital" => "Brasilia",
            "moneda"  =>  "Real",
            "poblacion"  => 212.216
                   ],
        "Bolivia" =>[ 
            "capital" => "La paz",
            "moneda"  =>  "Boliviano",
            "poblacion"  => 11.633
                    ]
    ];

    //recorrer la primera dimension del arreglo

    //foreach ($paises as $pais => $infopais){
      //  echo "<h2> $pais </h2>";
     //   echo "capital:" .$infopais["capital"] . "<br/>";
    //     echo "moneda:" .$infopais["moneda"] . "<br/>";
      //  echo "población (en millones de habitantes):" .$infopais["poblacion"] . "<br/>";
    // echo "<hr />";
        
   // }
     
     //   echo "<pre>";
   // var_dump($paises);
    //echo "</pre>";

    //mostrar una vista para presentar paises
    // En MVC yo puedo pasar datos a una vista
    return view('paises')->with("paises",$paises)
    
    ;
  
});